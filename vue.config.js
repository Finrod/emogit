module.exports = {
  lintOnSave: false,

  css: {
    sourceMap: true
  },

  publicPath: process.env.NODE_ENV === 'production'
    ? '/emogit/'
    : '/',

  pwa: {
    name: 'EM😋GIT',
    themeColor: '#FFF9C4'
  }
}
