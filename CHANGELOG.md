# CHANGELOG

## v2.0.2
 * 🔖 Release 2.0.2
 * 🐛 Fix copy not working

## v2.0.1
 * 🔖 Release 2.0.1
 * ✅ Code quality
 * ⏫ Update dependencies
 * 🚸 Improve scolling
 * 🐛 Fix missing parameter for removeEventListener
 * 🐛 Fix hideMainWindow not defined

## v2.0.0
 * 🔖 Release 2.0.0
 * ♻️ Convert to PWA
 * 📦 Track release files as LFS

## v1.0.1
 * 🔖 Release 1.0.1
 * 🐛 Fix window frame displayed Close #3
 * ✅ Code quality
 * 🚸 Hide window on click Close #2
 * 🚸 Improve scrolling view
 * ⏫ Update dependencies
 * 📄 Update LICENSE
 * 📝 Add CHANGELOG
 * 📄 Add LICENSE

## v1.0.0
 * 🔖 Release 1.0.0
 * 🏗 Transform to an electron application
 * 🔒 Add https redirection
 * 💄 Add pointer cursor to title
 * 🚸 Fix width to be contained in Firefox docked tab
 * ✨ Reset search on a click on the title
 * 🐛 Fix search being case sensitive
 * 👷 Update CI build
 * 🎉 Initial commit
